//
//  ViewController.m
//  BLETest01
//
//  Created by 舛田 明寛 on 2015/03/29.
//  Copyright (c) 2015年 AkihiroMasuda. All rights reserved.
//

#import "ViewController.h"
#import <CoreBluetooth/CoreBluetooth.h>
#import "define.h"

// サービスUUID:Immediate Alert
NSString *kUUIDServiceImmediateAlert = @"1802";
// サービスUUID:Battery Service
NSString *kUUIDServiceBatteryService = @"180F";
// キャラクタリスティックUUID:Alert Level
NSString *kUUIDCharacteristicsAlertLevel = @"2A06";
// キャラクタリスティックUUID:Battery Level
NSString *kUUIDCharacteristicsBatteryLevel = @"2A19";


// RBL Service
#define RBL_SERVICE_UUID                         @"713D0000-503E-4C75-BA94-3148F18D941E"
#define RBL_CHAR_TX_UUID                         @"713D0002-503E-4C75-BA94-3148F18D941E"
#define RBL_CHAR_RX_UUID                         @"713D0003-503E-4C75-BA94-3148F18D941E"
#define RBL_BLE_FRAMEWORK_VER                    0x0200
#define RBL_BLEND_MICRO_LOCAL_NAME                    @"BlendMicro"

// 接続状態の定義
NSString *kConnectionDisconnected = @"未接続／接続切れ";
NSString *kConnectionConnecting = @"接続中";
NSString *kConnectionSearchServices = @"サービス取得中";
NSString *kConnectionSearchCharacteristics = @"Characteristic取得中";
NSString *kConnectionConnected = @"接続完了";


@interface ViewController ()<CBCentralManagerDelegate, CBPeripheralDelegate>
@property dispatch_queue_t centralManagerSerialGCDQueue;
@property CBCentralManager* centralManager;
@property NSMutableSet* peripherals;
@property NSTimer* tm;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    // UI調整
    int sizeLabelFont = 0;
    int sizeButtonFont = 0;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone){
        // iPhone
        sizeLabelFont = 32;
        sizeButtonFont = 32;
    }
    else{
        // iPad
        sizeLabelFont = 64;
        sizeButtonFont = 32;
    }
    _lblCurrentStatus.font = [UIFont systemFontOfSize:sizeLabelFont];
    _btn1.titleLabel.font = [UIFont systemFontOfSize:sizeButtonFont];
    [_btn1 setTitle:@"スキャン再実行" forState:UIControlStateNormal];

    [self initBLE];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)buttonDidTouched:(UIButton *)sender {
    // スキャン再実行
    [self startScanBLE];
}

- (void)initBLE
{
    LOG_METHOD_IN;
    /// BLE 通信全般の処理が実行される queue を第 2 引数としてわたす。nil を渡した場合は mainQueue で実行される。
    _centralManagerSerialGCDQueue = dispatch_queue_create("com.akidn8.bletest01", DISPATCH_QUEUE_SERIAL);
    self.centralManager = [[CBCentralManager alloc] initWithDelegate:self queue:_centralManagerSerialGCDQueue];
    
    /// 接続するペリフェラルを保持するためのセットを生成
    self.peripherals = [NSMutableSet set];
    
    /// UI更新
    [self setLabelTxtAsync:kConnectionDisconnected];
}

// BLEスキャン開始
- (void)startScanBLE
{
    LOG_METHOD_IN;
    [self.centralManager scanForPeripheralsWithServices:nil options:nil];
}

// BLEスキャン停止
- (void)stopScanBLE
{
    LOG_METHOD_IN;
    [self.centralManager stopScan];
}

// Peripheralとコネクション開始
- (void)connectPeripheral:(CBPeripheral*) peripheral
{
    LOG_METHOD_IN;
    [self.centralManager connectPeripheral:peripheral options:nil];
    /// UI更新
    [self setLabelTxtAsync:kConnectionConnecting];
}

// Peripheralが提供するサービスを探す
- (void)discoverServicesWithPeripheral:(CBPeripheral*)peripheral
{
    LOG_METHOD_IN;
    //    [peripheral discoverServices:@[[CBUUID UUIDWithString:@"ffe0"]]];
    //    [peripheral discoverServices:@[[CBUUID UUIDWithString:kUUIDServiceBatteryService]]];
    // 引数にnilを渡すとすべてのサービスを検索。非推奨らしいが。
    [peripheral discoverServices:nil];
    /// UI更新
    [self setLabelTxtAsync:kConnectionSearchServices];
}

// Peripheralのサービスに対してCharacteristicを検索
- (void)discoverCharacteristicsWithPeripheral:(CBPeripheral*)peripheral
{
    LOG_METHOD_IN;
    for (CBService * service in peripheral.services) {
        NSLog([service description]);
        [peripheral discoverCharacteristics:nil forService:service];
    }
    /// UI更新
    [self setLabelTxtAsync:kConnectionSearchCharacteristics];
}

// PeripheralのCharacteristicに対しREADを発行
- (void)readForCharacteristicWithPeripheral:(CBPeripheral*)peripheral characteristic:(CBCharacteristic*)characteristic
{
    LOG_METHOD_IN;
    [peripheral readValueForCharacteristic:characteristic];
}

- (bool)isEqualCharacteristic:(CBCharacteristic*)characteristic uuidStr:(NSString*)uuidStr
{
    return [characteristic.UUID isEqual:[CBUUID UUIDWithString:uuidStr]];
}

#pragma mark CBCentralManagerDelegate
- (void)centralManagerDidUpdateState:(CBCentralManager *)central
{
    LOG_METHOD_IN;
    NSLog(@"centralManagerDidUpdateState %d", central.state);
    switch (central.state) {
        case CBCentralManagerStatePoweredOff:
            break;
            
        case CBCentralManagerStatePoweredOn:
            [self startScanBLE];
            break;
            
        default:
            break;
    }
}

// アドバタイザなど拾ってPeripheralを検出したときのコールバック
- (void)centralManager:(CBCentralManager *)central
 didDiscoverPeripheral:(CBPeripheral *)peripheral
     advertisementData:(NSDictionary *)advertisementData
                  RSSI:(NSNumber *)RSSI
{
    LOG_METHOD_IN;
    NSString *localName = [advertisementData objectForKey:CBAdvertisementDataLocalNameKey];
    NSLog(@"Peripheral localName:%@ RSSI:%@", localName, RSSI);
//    if ([localName length] && [localName rangeOfString:@"HRM1017_HTM"].location != NSNotFound) {
//    if ([localName length] && [localName rangeOfString:RBL_BLEND_MICRO_LOCAL_NAME].location != NSNotFound) {
    bool isFound = ([localName length] && [localName rangeOfString:@"HRM1017_HTM"].location != NSNotFound) ||
    ([localName length] && [localName rangeOfString:RBL_BLEND_MICRO_LOCAL_NAME].location != NSNotFound);
    if (isFound){
    
    
        //        [self updateLabel:self.connectionStatusLabel text:@"接続開始"];
        
        /// Scan を停止させる
        [self stopScanBLE];
        
        /// CBPeripheral のインスタンスを保持しなければならない
        [self.peripherals addObject:peripheral];
        
        /// コネクション開始
        [self connectPeripheral:peripheral];
        
    }
}

// Peripheralとの接続が完了したときのコールバック
- (void)centralManager:(CBCentralManager *)central
  didConnectPeripheral:(CBPeripheral *)peripheral
{
    LOG_METHOD_IN;
    /// Peripheral にデリゲートをセットし、Service を探す。今回はボタン押下のサービスのみを探す
    peripheral.delegate = self;
    
    [self discoverServicesWithPeripheral:peripheral];
}

// Peripheralとの接続が切れた時のコールバック
- (void)centralManager:(CBCentralManager *)central didDisconnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error
{
    LOG_METHOD_IN;
    /// UI更新
    [self setLabelTxtAsync:kConnectionDisconnected];
    
    // スキャン再実行
    [self startScanBLE];
}


#pragma mark CBPeripheralDelegate
// PeripheralのServiceが取得できたときのコールバック
- (void)peripheral:(CBPeripheral *)peripheral didDiscoverServices:(NSError *)error
{
    LOG_METHOD_IN;
    // ログ出力
    for (CBService *service in peripheral.services) {
        NSLog(@"service : %@", [service description]);
    }
    
    /// Characteristic を全て探す
    [self discoverCharacteristicsWithPeripheral:peripheral];
}

static CBPeripheral* p;
static CBCharacteristic* c;

// PeripheralのCharacteristicsが取得できたときのコールバック
- (void)peripheral:(CBPeripheral *)peripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error
{
    LOG_METHOD_IN;
    /// Characteristic に対して Notify を受け取れるようにする
    for (CBService *service in peripheral.services) {
        for (CBCharacteristic *characteristic in service.characteristics) {
            NSLog(@"characteristic : %@", [characteristic description]);
            [peripheral setNotifyValue:YES forCharacteristic:characteristic];
            
            //            // 試しにReadを投げる
            //            [self readForCharacteristicWithPeripheral:peripheral characteristic:characteristic];
            //
            //            if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:kUUIDCharacteristicsBatteryLevel]]){
            //                static bool flg = false;
            //                if (!flg){
            //                    flg = true;
            //                    dispatch_queue_t mainQ = dispatch_get_main_queue();
            //                    dispatch_async(mainQ, ^{
            //                        _tm = [NSTimer scheduledTimerWithTimeInterval:1.5f target:self selector:@selector(hoge:) userInfo:nil repeats:true];
            //                        p = peripheral;
            //                        c = characteristic;
            //                        //                    [_tm fire];
            //                    });
            //
            //                }
            //            }
            
        }
    }
    /// UI更新
    [self setLabelTxtAsync:kConnectionConnected];
    
}

// PeripheralからのCharacteristicsの値を取得できたときのコールバック
// 呼ばれるタイミングは以下の２パターンがある
//  - CentralからのReadRequestの応答時
//  - PeripheralからのNotify受信時
- (void)peripheral:(CBPeripheral *)peripheral
didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic
             error:(NSError *)error
{
    LOG_METHOD_IN;
    NSLog(@"characteristic : %@", characteristic);
    // 1バイトデータを取得してみる
    UInt8 val = 0;
    [characteristic.value getBytes:&val length:1];
//    if ([self isEqualCharacteristic:characteristic uuidStr:kUUIDCharacteristicsBatteryLevel]){
//    if ([self isEqualCharacteristic:characteristic uuidStr:RBL_CHAR_TX_UUID]){
    bool isFound =([self isEqualCharacteristic:characteristic uuidStr:kUUIDCharacteristicsBatteryLevel]) ||([self isEqualCharacteristic:characteristic uuidStr:RBL_CHAR_TX_UUID]);
    if (isFound){
        dispatch_queue_t mainQ = dispatch_get_main_queue();
        dispatch_async(mainQ, ^{
            _lblCurrentStatus.text = [NSString stringWithFormat:@"%d", val];
            NSLog(@"%d", val);
        });
    }
}

#pragma mark else
-(void)hoge:(NSTimer*)timer
{
    // 試しにReadを投げる
    [self readForCharacteristicWithPeripheral:p characteristic:c];
}

-(void)setLabelTxtAsync:(NSString*)str
{
    dispatch_queue_t mainQ = dispatch_get_main_queue();
    dispatch_async(mainQ, ^{
        _lblCurrentStatus.text = str;
    });
}


@end
