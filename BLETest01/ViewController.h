//
//  ViewController.h
//  BLETest01
//
//  Created by 舛田 明寛 on 2015/03/29.
//  Copyright (c) 2015年 AkihiroMasuda. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *lblCurrentStatus;
@property (weak, nonatomic) IBOutlet UIButton *btn1;
- (IBAction)buttonDidTouched:(UIButton *)sender;

@end

